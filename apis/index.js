var express = require("express");
var ith = require('./routers/ith');
var rfid = require('./routers/rfid');
var category = require('./routers/category');
var observation = require('./routers/observation');

var app = express();

app.use('/ith', ith);
app.use('/rfid', rfid);
app.use('/category', category);
app.use('/observation', observation);

app.listen(1026, () => {
    console.log("El servidor está inicializado en el puerto 1026");
});