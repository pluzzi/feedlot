var express = require("express");
var mysql = require('mysql');
var config = require('../config/database-config');
var bodyParser = require('body-parser');

var router = express.Router();

router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

router.use(function timeLog(req, res, next) {
    console.log('Time: ', Date.now());
    next();
});

router.all('*', function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
});

router.get('/', function (req, res) {
    var parameters = config.getParameters();
    var con = mysql.createConnection(parameters);

    con.connect(function(err) {
        if (err) throw err;
    });

    var sql = `select * from rfid`;

    con.query(sql, function (err, result, fields) {
        if (err) throw err;

        res.send(result);
    });
});

router.get('/:rfid', function (req, res) {
    var parameters = config.getParameters();
    var con = mysql.createConnection(parameters);

    con.connect(function(err) {
        if (err) throw err;
    });

    var sql = `select * from rfid where rfid like '%${req.params.rfid}%' limit 1`;

    con.query(sql, function (err, result, fields) {
        if (err) throw err;

        res.send(result[0]);

    });
});

router.post('/', function (req, res) {
    var parameters = config.getParameters();
    var con = mysql.createConnection(parameters);

    con.connect(function(err) {
        if (err) throw err;
    });

    //sql = `select count(1) as count from rfid where rfid='${req.body.rfid}'`;
    
    //con.query(sql, function (err, result) {
    //    if (err) throw err;
        
    //   if(result[0].count != 0){
    //        res.status(200).send("El tag ya existe.");
    //        return;
    //    }
        
        var sql = `insert into rfid (rfid) values ('${req.body.rfid}')`;

        con.query(sql, function (err, result, fields) {
            if (err) throw err;

            var object = {};

            var key = 'id';
            object[key] = result.insertId;

            key = 'rfid';
            object[key] = req.body.rfid;

            res.send(object);
        });
        
    //});
});

    
router.put('/:id', function (req, res) {
    var parameters = config.getParameters();
    var con = mysql.createConnection(parameters);

    con.connect(function(err) {
        if (err) throw err;
    });

    var sql = `update rfid set rfid='${req.body.rfid}' where id=${req.params.id}`;

    con.query(sql, function (err, result, fields) {
        if (err) throw err;

        res.send(result);
    });
});

router.delete('/:id', function (req, res) {
    var parameters = config.getParameters();
    var con = mysql.createConnection(parameters);

    con.connect(function(err) {
        if (err) throw err;
    });

    var sql = `delete from rfid where id=${req.params.id}`;

    con.query(sql, function (err, result, fields) {
        if (err) throw err;

        res.send(result);
    });
});

module.exports = router;