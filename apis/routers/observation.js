var express = require("express");
var mysql = require('mysql');
var config = require('../config/database-config');
var bodyParser = require('body-parser');

var router = express.Router();

router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

router.use(function timeLog(req, res, next) {
    console.log('Time: ', Date.now());
    next();
});

router.all('*', function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
});

router.get('/', function (req, res) {
    var parameters = config.getParameters();
    var con = mysql.createConnection(parameters);

    con.connect(function(err) {
        if (err) throw err;
    });

    var sql = `select * from observations`;

    con.query(sql, function (err, result, fields) {
        if (err) throw err;

        res.send(result);
    });
});

router.get('/:rfid', function (req, res) {
    var parameters = config.getParameters();
    var con = mysql.createConnection(parameters);

    con.connect(function(err) {
        if (err) throw err;
    });

    var sql = `select * from observations where rfid_id ='${req.params.rfid}'`;

    con.query(sql, function (err, result, fields) {
        if (err) throw err;

        res.send(result);
    });
});

router.post('/', function (req, res) {
    var parameters = config.getParameters();
    var con = mysql.createConnection(parameters);

    con.connect(function(err) {
        if (err) throw err;
    });

    var sql = `insert into observations (rfid_id,observation)
               values ('${req.body.rfid_id}','${req.body.observation}')`;

    con.query(sql, function (err, result, fields) {
        if (err) throw err;

        res.send(result);
    });
});

router.put('/:id', function (req, res) {
    var parameters = config.getParameters();
    var con = mysql.createConnection(parameters);

    con.connect(function(err) {
        if (err) throw err;
    });

    var sql = `update observations set observation='${req.body.observation}' where id=${req.params.id}`;

    con.query(sql, function (err, result, fields) {
        if (err) throw err;

        res.send(result);
    });
});

router.delete('/:id', function (req, res) {
    var parameters = config.getParameters();
    var con = mysql.createConnection(parameters);

    con.connect(function(err) {
        if (err) throw err;
    });

    var sql = `delete from observations where id=${req.params.id}`;

    con.query(sql, function (err, result, fields) {
        if (err) throw err;

        res.send(result);
    });
});

module.exports = router;