var express = require("express");
var mysql = require('mysql');
var config = require('../config/database-config');

var router = express.Router();

router.use(function timeLog(req, res, next) {
    console.log('Time: ', Date.now());
    next();
});

router.all('*', function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
});

router.get('/', function (req, res) {
    var parameters = config.getParameters();
    var con = mysql.createConnection(parameters);

    con.connect(function(err) {
        if (err) throw err;
    });

    var sql = `(SELECT value FROM history h
                LEFT JOIN io c on c.id = h.io_id
                WHERE c.sort_description = 'Ai1'
                ORDER BY h.create_time DESC LIMIT 1)
               UNION ALL
               (SELECT value FROM history h 
                LEFT JOIN io c on c.id = h.io_id
                WHERE c.sort_description = 'Ai2'
                ORDER BY h.create_time DESC LIMIT 1)`;

    con.query(sql, function (err, result, fields) {
        if (err) throw err;

        var object = {};
        var key = 'humidity';
        object[key] = result[1]['value'];

        key = 'temperature';
        object[key] = result[0]['value'];

        key = 'ith';
        var ith = ((1.8*result[0]['value']) + 32)-(0.55 - 0.55*result[1]['value']/100) * (1.8*result[0]['value'] - 26);
        object[key] = parseFloat(ith.toFixed(2));

        var alerts = [];
        var a1 = {
            code: '1',
            description: 'No Estres Calórico',
            range: '0-74',
            active: ith <= 74 
        };
        alerts.push(a1);

        var a2 = {
            code: '2',
            description: 'Leve Estres Calórico',
            range: '74-80',
            active: ith > 74 && ith <= 80
        };
        alerts.push(a2);

        var a3 = {
            code: '3',
            description: 'Estres Calórico Medio',
            range: '80-84',
            active: ith > 80 && ith <= 84
        };
        alerts.push(a3);

        var a4 = {
            code: '4',
            description: 'Estres Calórico Grave',
            range: '85+',
            active: ith >= 85
        };
        alerts.push(a4);

        key = "alerts";
        object[key]= alerts;

        res.send(object);
    });
});

module.exports = router;