<section id="work" class="padd-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="heading-sec">Trabajos recientes</h3>
            </div>
        </div><!--row-->
    </div>
    <div id="js-filters-mosaic-flat" class="cbp-l-filters-buttonCenter">
        <div data-filter="*" class="cbp-filter-item-active cbp-filter-item">
            All <div class="cbp-filter-counter"></div>
        </div>
        <div data-filter=".print" class="cbp-filter-item">
            Print <div class="cbp-filter-counter"></div>
        </div>
        <div data-filter=".web-design" class="cbp-filter-item">
            Web Design <div class="cbp-filter-counter"></div>
        </div>
        <div data-filter=".graphic" class="cbp-filter-item">
            Graphic <div class="cbp-filter-counter"></div>
        </div>
        <div data-filter=".motion" class="cbp-filter-item">
            Motion <div class="cbp-filter-counter"></div>
        </div>
    </div>

    <div id="js-grid-mosaic-flat" class="cbp cbp-l-grid-mosaic-flat">
        <div class="cbp-item web-design graphic">
            <a href="assets/img/work/work-1.jpg" class="cbp-caption cbp-lightbox" data-title="Bolt UI<br>by Tiberiu Neamu">
                <div class="cbp-caption-defaultWrap">
                    <img src="assets/img/work/mosaic1.jpg" alt="">
                </div>
                <div class="cbp-caption-activeWrap">
                    <div class="cbp-l-caption-alignCenter">
                        <div class="cbp-l-caption-body">
                            <div class="cbp-l-caption-title">Bolt UI</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="cbp-item print motion">
            <a href="assets/img/work/work-2.jpg" class="cbp-caption cbp-lightbox" data-title="World Clock<br>by Paul Flavius Nechita">
                <div class="cbp-caption-defaultWrap">
                    <img src="assets/img/work/mosaic2.jpg" alt="">
                </div>
                <div class="cbp-caption-activeWrap">
                    <div class="cbp-l-caption-alignCenter">
                        <div class="cbp-l-caption-body">
                            <div class="cbp-l-caption-title">World Clock</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="cbp-item print motion">
            <a href="assets/img/mas-images/img-8.jpg" class="cbp-caption cbp-lightbox" data-title="WhereTO App<br>by Tiberiu Neamu">
                <div class="cbp-caption-defaultWrap">
                    <img src="assets/img/work/mosaic-lg1.jpg" alt="">
                </div>
                <div class="cbp-caption-activeWrap">
                    <div class="cbp-l-caption-alignCenter">
                        <div class="cbp-l-caption-body">
                            <div class="cbp-l-caption-title">WhereTO App</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="cbp-item motion graphic">
            <a href="assets/img/mas-images/img-6.jpg" class="cbp-caption cbp-lightbox" data-title="Seemple* Music<br>by Tiberiu Neamu">
                <div class="cbp-caption-defaultWrap">
                    <img src="assets/img/work/mosaic-hor1.jpg" alt="">
                </div>
                <div class="cbp-caption-activeWrap">
                    <div class="cbp-l-caption-alignCenter">
                        <div class="cbp-l-caption-body">
                            <div class="cbp-l-caption-title">Seemple* Music</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="cbp-item web-design print">
            <a href="assets/img/mas-images/img-11.jpg" class="cbp-caption cbp-lightbox" data-title="iDevices<br>by Tiberiu Neamu">
                <div class="cbp-caption-defaultWrap">
                    <img src="assets/img/work/mosaic-hor2.jpg" alt="">
                </div>
                <div class="cbp-caption-activeWrap">
                    <div class="cbp-l-caption-alignCenter">
                        <div class="cbp-l-caption-body">
                            <div class="cbp-l-caption-title">iDevices</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="cbp-item print motion">
            <a href="assets/img/work/work-6.jpg" class="cbp-caption cbp-lightbox" data-title="Remind~Me Widget<br>by Tiberiu Neamu">
                <div class="cbp-caption-defaultWrap">
                    <img src="assets/img/work/mosaic-hor3.jpg" alt="">
                </div>
                <div class="cbp-caption-activeWrap">
                    <div class="cbp-l-caption-alignCenter">
                        <div class="cbp-l-caption-body">
                            <div class="cbp-l-caption-title">Remind~Me Widget</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="cbp-item web-design print">
            <a href="assets/img/work/img-4.jpg" class="cbp-caption cbp-lightbox" data-title="Workout Buddy<br>by Tiberiu Neamu">
                <div class="cbp-caption-defaultWrap">
                    <img src="assets/img/work/mosaic3.jpg" alt="">
                </div>
                <div class="cbp-caption-activeWrap">
                    <div class="cbp-l-caption-alignCenter">
                        <div class="cbp-l-caption-body">
                            <div class="cbp-l-caption-title">Workout Buddy</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="cbp-item print graphic">
            <a href="assets/img/work/work-6.jpg" class="cbp-caption cbp-lightbox" data-title="Digital Menu<br>by Cosmin Capitanu">
                <div class="cbp-caption-defaultWrap">
                    <img src="assets/img/work/mosaic4.jpg" alt="">
                </div>
                <div class="cbp-caption-activeWrap">
                    <div class="cbp-l-caption-alignCenter">
                        <div class="cbp-l-caption-body">
                            <div class="cbp-l-caption-title">Digital Menu</div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>



</section>