<div id="services" class="padd-section">
    <section id="content-region-1" class="padding-40">
        <div class="container">
            <div class="row">
                <div class="col-md-7 features text-xs-center">
                    <h3>Productos y servicios</h3>
                    
                    <div class="space-30"></div>

                    <?php
                        include('config/database-config.php');
                            
                        $sql = "select title, id, icon, substring(description,1,150) as description, logo from products limit 3";

                        $products = $conn->query($sql);

                        while ($product = mysqli_fetch_assoc($products)) {
                            echo '<div class="row">
                                    <div class="col-md-3 services-icon">
                                        <img class="img-fluid" alt="" src="data:image/jpeg;base64,'.base64_encode( $product['logo'] ).'"/>
                                    </div>
                                    <div class="col-md-9 services-text">
                                        <h4 class="heading-mini">'.$product['title'].'</h4>
                                        <p>'.$product['description'].'...</p>
                                        <a href="product.php?id='.$product['id'].'" class="btn theme-btn-default btn-md"> Ver más</a>
                                    </div>
                                </div>
                                <div class="space-30"></div>';
                        }
                    ?>


                
                </div>
                <div class="col-md-4">
                    <?php
                        $sql = "select img  from configuration limit 1";

                        $result = $conn->query($sql);

                        $row = mysqli_fetch_assoc($result);
                        
                        echo '<img class="img-fluid" alt="" src="data:image/jpeg;base64,'.base64_encode( $row['img'] ).'"/>';
                        
                    ?>
                </div>
            </div>
        </div><!--container-->
    </section><!--features end-->
</div>