<section id="home">
            <div style="height:50px"></div>
            <div class="clearfix"></div>
            <div class="slider-master">
                <!-- Master Slider -->
                <div class="master-slider ms-skin-default" id="masterslider"> 
                    <?php
                        include('config/database-config.php');
                        
                        $sql = "SELECT * FROM sliders";
                        $result = $conn->query($sql);

                        while($row = $result->fetch_assoc()){
                            echo '<div class="ms-slide slide-1" data-delay="14"> 
                                    <img alt="Slide1 background" id="preview" src="data:image/jpeg;base64,'.base64_encode( $row['img'] ).'" />

                                    <h3 class="ms-layer title_main"
                                        style="left:150px;top: 230px;"
                                        data-type="text"
                                        data-delay="1500"
                                        data-duration="2500"
                                        data-ease="easeOutExpo"
                                        data-effect="rotate3dtop(-100,0,0,40,t)">'.$row['title'].'</h3>
                                    <h5 class="ms-layer sub-title"
                                        style="left:150px; top: 320px;"
                                        data-type="text"
                                        data-effect="bottom(45)"
                                        data-duration="3000"
                                        data-delay="2000"
                                        data-ease="easeOutExpo">'.$row['subtitle'].'</h5>
                                </div>';
                        }

                    ?>

                </div>
                <!-- end Master Slider -->
            </div>
        </section>
