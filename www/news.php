<div id="newsletter" class="newsletter-section">
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <h3>Mantente informado</h3>
                <p>
                    Suscríbete a nuestro newsletter
                </p>
            </div>
        </div><!--row-->

        <div class="row">
            <div class="col-md-4 offset-md-4 text-center">
                <form method="POST" action="add-contact-newsletter.php" class="form-subscribe">
                    <input type="text" name="email" placeholder="Email..." class="form-control">
                    <button type="submit" name="submit" class="btn theme-btn-color btn-block">Suscríbete</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="space-70"></div>

<?php
    include('config/database-config.php');

    $sql = "select 
    p.id, p.img, p.title, p.author, c.description, DATE_FORMAT(p.create_date, '%M %d %Y') as fecha, substring(body, 1, 150) as body, p.video
    from posts p left join post_category c on p.categoryid=c.id limit 6";

    $result = $conn->query($sql);

    $count = $result->num_rows;

?>


<div class="container">
    <div class="row">
        <div class="col-md-12">
            <?php if($count!=0){ echo '<h3 class="heading-sec">Ultimas noticias</h3>'; } ?>
        </div>
    </div><!--row-->
    <div class="row">
        <?php
            while ($row = mysqli_fetch_assoc($result)) {
                echo '<div class="col-md-4 col-sm-6 margin-btm-20">
                        <div class="news-sec">
                            <div class="news-thumnail">
                                <a href="blog-post.php?id='.$row['id'].'">';
                                    if($row['video']!=""){
                                        echo '<iframe width="100%" height="200" alt="" src="'.$row['video'].'" ></iframe>';
                                    }else{
                                        echo '<img class="img-fluid" alt="" src="data:image/jpeg;base64,'.base64_encode( $row['img'] ).'" />';
                                    }
                                    
                echo            '</a>
                            </div>
                            <div class="news-desc" style="word-wrap: break-word;">
                                <h3 class="blog-post-title"><a href="blog-post.php?id='.$row['id'].'" class="hover-color">'.$row['title'].'</a></h3>
                                <span class="news-post-cat">'.$row['fecha'] .' | '.$row['description'] .'</span>
                                <p>
                                    '.$row['body'].'...
                                </p>
                            </div>
                        </div>
                    </div>';
            }
        ?>
    </div><!--row end-->
</div><!--container recent news end-->