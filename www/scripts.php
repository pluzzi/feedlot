<!-- jQuery plugins. -->
<script src="assets/js/plugins/plugins.js"></script>
<script src="assets/js/app.js"></script>

<!--page template scripts-->
<script src="assets/masterslider/masterslider.min.js"></script> 
<script src="assets/js/masterslider-custom.js" type="text/javascript"></script>
<script src="assets/cubeportfolio/js/jquery.cubeportfolio.min.js"></script> 

<script>
    //auto close navbar-collapse on click a
    $('a.nav-link').on('click', function () {
        $('.navbar-toggler:visible').click();
    });
    (function ($, window, document, undefined) {
        'use strict';

        // init cubeportfolio
        $('#js-grid-mosaic-flat').cubeportfolio({
            filters: '#js-filters-mosaic-flat',
            layoutMode: 'mosaic',
            sortToPreventGaps: true,
            mediaQueries: [{
                    width: 1500,
                    cols: 6
                }, {
                    width: 1100,
                    cols: 4
                }, {
                    width: 800,
                    cols: 3
                }, {
                    width: 480,
                    cols: 2,
                    options: {
                        caption: '',
                        gapHorizontal: 15,
                        gapVertical: 15
                    }
                }],
            defaultFilter: '*',
            animationType: 'fadeOutTop',
            gapHorizontal: 0,
            gapVertical: 0,
            gridAdjustment: 'responsive',
            caption: 'fadeIn',
            displayType: 'fadeIn',
            displayTypeSpeed: 100,
            // lightbox
            lightboxDelegate: '.cbp-lightbox',
            lightboxGallery: true,
            lightboxTitleSrc: 'data-title',
            lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>',
        });
    })(jQuery, window, document);
</script>