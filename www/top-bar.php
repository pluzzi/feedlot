<?php
    include('config/database-config.php');
                
    $sql = "select * from configuration limit 1";

    $result = $conn->query($sql);
    $config = mysqli_fetch_assoc($result);

?>

<a href="#" class="scrollToTop"><i class="ion-android-arrow-dropup-circle"></i></a>

<!--back to top end-->
<nav class="navbar navbar-toggleable-sm navbar-light fixed-top bg-faded search-header">
    <div class="container">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <a class="navbar-brand" href="index.php">
            <?php
                echo '<img style="width: 200px; heigth:75px;" alt="" src="data:image/jpeg;base64,'.base64_encode( $config['darklogo'] ).'"/>';
            ?>
        </a>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="nav navbar-nav ml-auto smooth-scroll">
                <li class="nav-item"><a class="nav-link" href="index.php#home"><i class="ion-home"></i> Inicio</a></li>
                <li class="nav-item"><a class="nav-link" href="index.php#about"><i class="ion-person-stalker"></i> Empresa</a></li>
                <!--<li class="nav-item"><a class="nav-link" href="#work"><i class="ion-images"></i> Portfolio</a></li>-->
                <li class="nav-item"><a class="nav-link" href="index.php#services"><i class="ion-settings"></i> Productos y Servicios</a></li>
                <li class="nav-item"><a class="nav-link" href="index.php#newsletter"><i class="ion-android-mail"></i> Noticias</a></li>
                <li class="nav-item"><a class="nav-link" href="index.php#our-team"><i class="ion-person"></i> Nosotros</a></li>
                <li class="nav-item"><a class="nav-link" href="index.php#contact"><i class="ion-android-call"></i> Contacto</a></li>

            </ul>
        </div>

    </div>

</nav><!--nav end-->