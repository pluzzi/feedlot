<?php
    include('config/database-config.php');
                
    $sql = "select * from configuration limit 1";

    $result = $conn->query($sql);
    $row = mysqli_fetch_assoc($result);

?>

<section id="contact">
    <div class="space-50"></div>
    <div id="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4 margin-btm-20">
                    <div class="footer-col">
                        <h3>Información de contacto</h3>
                        <p><i class="ion-home"></i> <?php echo $row['address']; ?></p>
                        <p><i class="ion-iphone"></i> <?php echo $row['phone']; ?></p>
                        <p><i class="ion-email"></i> <?php echo $row['email']; ?></p>
                    </div>
                    <div class="space-20"></div>
                    <div class="footer-col">
                        <h3>Siguenos</h3>
                        <ul class=" list-inline social-btn">
                            <li class="list-inline-item"><a href="<?php echo $row['facebook']; ?>"><i class="ion-social-facebook" data-toggle="tooltip" data-placement="top" title="" data-original-title="Facebook"></i></a></li>
                            <li class="list-inline-item"><a href="<?php echo $row['twitter']; ?>"><i class="ion-social-twitter" data-toggle="tooltip" data-placement="top" title="" data-original-title="Twitter"></i></a></li>
                            <li class="list-inline-item"><a href="<?php echo $row['linkedin']; ?>"><i class="ion-social-linkedin" data-toggle="tooltip" data-placement="top" title="" data-original-title="Linkedin"></i></a></li>
                            
                        </ul>
                    </div><!--footer social col-->
                </div><!--col-4 end-->
                
                <div class="col-md-8">
                    <div class="footer-col">
                        <h3>Contactanos</h3>
                        <form action="send-message.php" method="POST">
                            <div class="row">
                                <div class="col-md-6">
                                    <input name="name" type="text" class="form-control" placeholder="Nombre...">
                                </div>
                                <div class="col-md-6">
                                    <input name="email" type="email" class="form-control" placeholder="Email...">
                                </div>
                                <div class="col-md-12">
                                    <input name="title" type="text" class="form-control" placeholder="Título...">
                                </div>
                                <div class="col-md-12">
                                    <textarea name="message" class="form-control" placeholder="Mensaje..." rows="7"></textarea>
                                </div>
                                <div class="col-md-12 text-right">
                                    <button type="submit" class="btn btn-lg btn-white">Enviar Mensaje</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div><!--get in touch col end-->
            </div><!--footer main row end-->  
            <div class="space-70"></div>
            
            <?php include('footer.php'); ?>

        </div><!--container-->
    </div><!--footer main end-->
</section>