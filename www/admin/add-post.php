<?php include('islogin.php'); ?>

<?php
    $page_id = 3;
?>

<!DOCTYPE html>
<html>

<head>
    <?php include('headers.php'); ?>
    <link href="css/plugins/summernote/summernote.css" rel="stylesheet">
    <link href="css/plugins/summernote/summernote-bs3.css" rel="stylesheet">
</head>

<body>
    <div class="row">
        <div class="col-lg-12">
            <div id="wrapper">
                
                <?php include('nav-bar.php'); ?>

                <div id="page-wrapper" class="gray-bg dashbard-1">
                    <?php include('top-bar.php'); ?>
                    
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Nuevo Post</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>

                        <div class="ibox-content">
                            <div class="form-horizontal" >
                                <div class="form-group"><label class="col-sm-2 control-label">Título</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" id="title"></div>
                                </div>

                                <div class="hr-line-dashed"></div>

                                <div class="form-group"><label class="col-sm-2 control-label">Autor</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" id="author"></div>
                                </div>

                                <div class="hr-line-dashed"></div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Categoría</label>

                                    <div class="col-sm-10">
                                        <select class="form-control m-b" id="category">
                                            <?php 
                                                include('config/database-config.php');
                    
                                                $sql = "select * from post_category";
                                                $result = $conn->query($sql);
                                    
                                                while ($row = mysqli_fetch_assoc($result)) {
                                                    echo '<option data-id="'.$row['id'].'">'.$row['description'].'</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="wrapper wrapper-content">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="ibox float-e-margins">
                                                <div class="ibox-title">
                                                    <h5>Cuerpo del Post</h5>
                                                </div>
                                                <div class="ibox-content no-padding">
                                                    <div class="summernote">
                                                        
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Cropper -->
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title back-change">
                                        <h5>Imagen</h5>
                                    </div>
                                    <div class="ibox-content">
                                        <p>
                                            Seleccione la imagen para el Post.
                                        </p>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h4>Preview</h4>
                                                <img class="img-preview img-preview-sm" id="preview"/>
                                                
                                                <div class="btn-group">
                                                    <label title="Upload image file" for="inputImage" class="btn btn-primary">
                                                        <input onchange="readURL(this);" accept="image/*" type="file"  id="post-img">
                                                    </label>
                                                    
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group"><label class="col-sm-2 control-label">Video</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" id="video"></div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-4">
                                        <button class="btn btn-primary" id="save" >Guardar</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php include('footer.php'); ?>
        </div>
    </div>

    <?php include('scripts.php'); ?>

    <script src="js/plugins/summernote/summernote.min.js"></script>

    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $(document).ready(function() {
            $('.summernote').summernote({
                height: 130
            });

            $(document).on("click", "#save", function(){
                var title = $("#title").val();
                var author = $("#author").val();
                var category = $("#category").children(":selected").data("id");
                var body = $('.summernote').summernote('code');
                var video = $("#video").val();
                
                $.ajax({
                    url: "save-post.php",
                    method: "POST",
                    data: {title: title, author: author, category: category, body: body, video: video},
                    success: function(id){
                        var file_data = $("#post-img").prop('files')[0];   
                        var form_data = new FormData();                  
                        form_data.append('file', file_data);

                        $.ajax({
                            url: 'save-post-img.php?id='+id,
                            dataType: 'text',
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: form_data,                         
                            type: 'POST',
                            success: function(result){
                                window.location.href = "posts.php";
                            }
                        });
                    }
                });
            })

        });
    </script>

</body>

</html>