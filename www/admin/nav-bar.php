<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> <span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                    <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?php echo $_SESSION['name']; ?></strong></span>
                    <span class="text-muted text-xs block">Opciones <b class="caret"></b></span> </span> </a>
                    
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="logout.php">Logout</a></li>
                    </ul>
                </div>
            </li>
            <li <?php if($page_id==0){ echo "class='active'"; } ?>>
                <a href="main.php"><i class="fa fa-th-large"></i> <span class="nav-label">Mensajes</a>
            </li>
            <li <?php if($page_id==1){ echo "class='active'"; } ?>>
                <a href="users.php"><i class="fa fa-users"></i> <span class="nav-label">Usuarios</a>
            </li>
            <li <?php if($page_id==2){ echo "class='active'"; } ?>>
                <a href="news-letter.php"><i class="fa fa-th-large"></i> <span class="nav-label">Contactos News Letter</a>
            </li>
            <li <?php if($page_id==3){ echo "class='active'"; } ?>>
                <a href="posts.php"><i class="fa fa-th-large"></i> <span class="nav-label">Posts</a>
            </li>
            <li <?php if($page_id==4){ echo "class='active'"; } ?>>
                <a href="products.php"><i class="fa fa-th-large"></i> <span class="nav-label">Productos</a>
            </li>
            <li <?php if($page_id==5){ echo "class='active'"; } ?>>
                <a href="slider.php"><i class="fa fa-th-large"></i> <span class="nav-label">Slider</a>
            </li>
            <li <?php if($page_id==6){ echo "class='active'"; } ?>>
                <a href="team.php"><i class="fa fa-th-large"></i> <span class="nav-label">Equipo</a>
            </li>
            <li <?php if($page_id==7){ echo "class='active'"; } ?>>
                <a href="configuration.php"><i class="fa fa-th-large"></i> <span class="nav-label">Configuración</a>
            </li>
        </ul>

    </div>
</nav>