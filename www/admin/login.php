<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Datalogger | Login </title>
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">

    <!-- Toastr style -->
    <link href="css/plugins/toastr/toastr.min.css" rel="stylesheet">

</head>

<body class="gray-bg"> 
    <div class="loginColumns animated fadeInDown">
        <div class="row">
            <div class="col-md-6">
                <h2 class="font-bold">DASHBOARD</h2>
                <img src="img/logo.png" class="img-circle img-responsive csm-img-logo" >
            </div>
            <div class="col-md-6">
                <div class="ibox-content">
                    <form class="m-t" role="form" action="authenticate.php" method="POST">
                        <div class="form-group">
                            <input type="email" class="form-control" placeholder="Username" required="true" name="user">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" placeholder="Password" required="true" name="password">
                        </div>
                        <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

                     </form>
                    <p class="m-t">
                        <small>Patricio Luzzi &copy; 2019</small>
                    </p>
                </div>
            </div>
        </div>

    </div>

    <div class="footer">
        <div class="pull-right">
            10GB of <strong>250GB</strong> Free.
        </div>
        <div>
            <strong>Copyright</strong> Example Company &copy; 2014-2017
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <!-- Toastr script -->
    <script src="js/plugins/toastr/toastr.min.js"></script>

    <script>
        
        $(document).ready(function() {
            
            var $_GET = <?php echo json_encode($_GET); ?>;
            var login = $_GET['login'];

            if(login === '0'){
                toastr.warning('El usuario o la contraseña no son válidos.','Información');
            }

           

        })


    </script>

</body>

</html>

