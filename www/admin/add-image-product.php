<?php include('islogin.php'); ?>

<?php
    $page_id = 4;
?>

<!DOCTYPE html>
<html>

<head>
    <?php include('headers.php'); ?>
</head>

<body>
    <div class="row">
        <div class="col-lg-12">
            <div id="wrapper">
                
                <?php include('nav-bar.php'); ?>

                <div id="page-wrapper" class="gray-bg dashbard-1">
                    <?php include('top-bar.php'); ?>
                    
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Imágenes del Producto</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>

                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dataTables-example" >
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Producto</th>
                                            <th>Imagen</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody id="results">
                                        
                                    </tbody>
                                </table>
                            </div>

                        </div>

                        <div class="ibox float-e-margins">
                            <div class="ibox-title back-change">
                                <h5>Imagen</h5>
                            </div>
                            <div class="ibox-content">
                                <p>
                                    Seleccione la imagen para el Producto.
                                </p>
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4>Preview</h4>
                                        <img class="img-preview img-preview-sm" id="preview"/>
                                        
                                        <div class="btn-group">
                                            <label title="Upload image file" for="inputImage" class="btn btn-primary">
                                                <input onchange="readURL(this);" accept="image/*" type="file"  id="product-img">
                                            </label>
                                            
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <?php include('footer.php'); ?>
        </div>
    </div>

    <?php include('scripts.php'); ?>

    <script>
        function getImages(){
            var id = '<?php echo $_GET['id'] ?>';

            $.ajax({
                url: "get-images.php?id="+id,
                method: "POST",
                success: function(results){
                    $("#results").html(results);
                }
            })
        }

        function addImg(){
            var file_data = $("#product-img").prop('files')[0];   
            var form_data = new FormData();                  
            form_data.append('file', file_data);
            var id = '<?php echo $_GET['id'] ?>';

            $.ajax({
                url: 'save-product-img.php?id='+id,
                dataType: 'text',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         
                type: 'POST',
                success: function(result){
                    getImages();
                }
            });
        }

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);

                addImg();
            }
        }

        $(document).ready(function() {
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    
                ],
                searching: false,
                bInfo: false

            });
            
            $(document).on("click", "#delete", function(){
                var id = $(this).data("id");

                $.ajax({
                    url: "delete-image-product.php",
                    method: "POST",
                    data: {id: id},
                    success: function(results){
                        getImages();
                    }
                })
            })

            getImages();

        });
    </script>

</body>

</html>