<?php
    include('config/database-config.php');
        
    $sql = "select id, title, subtitle, img from sliders";

    $result = $conn->query($sql);

    while ($row = mysqli_fetch_assoc($result)) {
        echo '<tr>
                <td>'.$row['id'] .'</td>
                <td>'.$row['title'] .'</td>
                <td><img style="width: 100px; height: 70px !important;" class="img-preview img-preview-sm" src="data:image/jpeg;base64,'.base64_encode( $row['img'] ).'" /></td>
                <td>
                    <button id="delete" class="btn btn-primary btn-sm" data-id="'.$row['id'].'">
                        <i class="fa fa-minus-circle"></i>
                    </button>
                    <button id="edit" class="btn btn-primary btn-sm" data-id="'.$row['id'].'">
                        <i class="fa fa-edit"></i>
                    </button>
                </td>
            </tr>';
    }

?>