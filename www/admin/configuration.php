<?php include('islogin.php'); ?>

<?php
    $page_id = 7;

    include('config/database-config.php');
        
    $sql = "select * from configuration limit 1";
    $result = $conn->query($sql);
    $row = mysqli_fetch_assoc($result);

?>

<!DOCTYPE html>
<html>

<head>
    <?php include('headers.php'); ?>
</head>

<body>
    <div class="row">
        <div class="col-lg-12">
            <div id="wrapper">
                
                <?php include('nav-bar.php'); ?>

                <div id="page-wrapper" class="gray-bg dashbard-1">
                    <?php include('top-bar.php'); ?>
                    
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Editar Configuración</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>

                        <div class="ibox-content">
                            <div class="form-horizontal" >
                                <div class="form-group"><label class="col-sm-2 control-label">Dirección</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" id="address" value="<?php echo $row['address']; ?>"></div>
                                </div>

                                <div class="hr-line-dashed"></div>

                                <div class="form-group"><label class="col-sm-2 control-label">Teléfono</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" id="phone" value="<?php echo $row['phone']; ?>"></div>
                                </div>

                                <div class="hr-line-dashed"></div>

                                <div class="form-group"><label class="col-sm-2 control-label">E-Mail</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" id="email" value="<?php echo $row['email']; ?>"></div>
                                </div>

                                <div class="hr-line-dashed"></div>

                                <div class="form-group"><label class="col-sm-2 control-label">Facebook</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" id="facebook" value="<?php echo $row['facebook']; ?>"></div>
                                </div>

                                <div class="hr-line-dashed"></div>

                                <div class="form-group"><label class="col-sm-2 control-label">Twitter</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" id="twitter" value="<?php echo $row['twitter']; ?>"></div>
                                </div>

                                <div class="hr-line-dashed"></div>

                                <div class="form-group"><label class="col-sm-2 control-label">Linkedin</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" id="linkedin" value="<?php echo $row['linkedin']; ?>"></div>
                                </div>

                                <!-- Cropper -->
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title back-change">
                                        <h5>Logo</h5>
                                    </div>
                                    <div class="ibox-content">
                                        <p>
                                            Seleccione la imagen para el Logo.
                                        </p>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h4>Preview</h4>
                                                <?php echo '<img class="img-preview img-preview-sm" id="preview" src="data:image/jpeg;base64,'.base64_encode( $row['logo'] ).'" />'; ?>
                                                <div class="btn-group">
                                                    <label title="Upload image file" for="inputImage" class="btn btn-primary">
                                                        <input data-id="<?php echo $row['id']; ?>" onchange="readLogo(this);" accept="image/*" type="file"  id="logo-img">
                                                    </label>
                                                    
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="ibox float-e-margins">
                                    <div class="ibox-title back-change">
                                        <h5>Dark-Logo</h5>
                                    </div>
                                    <div class="ibox-content">
                                        <p>
                                            Seleccione la imagen para el DarkLogo.
                                        </p>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h4>Preview</h4>
                                                <?php echo '<img class="img-preview img-preview-sm" id="preview-darklogo" src="data:image/jpeg;base64,'.base64_encode( $row['darklogo'] ).'" />'; ?>
                                                <div class="btn-group">
                                                    <label title="Upload image file" for="inputImage" class="btn btn-primary">
                                                        <input data-id="<?php echo $row['id']; ?>" onchange="readDarkLogo(this);" accept="image/*" type="file"  id="darklogo-img">
                                                    </label>
                                                    
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="ibox float-e-margins">
                                    <div class="ibox-title back-change">
                                        <h5>Imagen</h5>
                                    </div>
                                    <div class="ibox-content">
                                        <p>
                                            Seleccione la imagen.
                                        </p>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h4>Preview</h4>
                                                <?php echo '<img class="img-preview img-preview-sm" id="preview-img" src="data:image/jpeg;base64,'.base64_encode( $row['img'] ).'" />'; ?>
                                                <div class="btn-group">
                                                    <label title="Upload image file" for="inputImage" class="btn btn-primary">
                                                        <input data-id="<?php echo $row['id']; ?>" onchange="readImg(this);" accept="image/*" type="file"  id="img">
                                                    </label>
                                                    
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php include('footer.php'); ?>
        </div>
    </div>

    <?php include('scripts.php'); ?>

    <script>
        function saveImg(input) {
            var file_data = input.prop('files')[0];   
            var form_data = new FormData();                  
            form_data.append('file', file_data);
            
            $.ajax({
                url: 'update-configuration-img.php?id='+input.attr('id'),
                dataType: 'text',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         
                type: 'POST',
                success: function(php_script_response){
                    //alert(php_script_response); 
                }
            });
        }

        function readLogo(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);

                saveImg($('#logo-img'));
            }
        }

        function readDarkLogo(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview-darklogo').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);

                saveImg($('#darklogo-img'));
            }
        }

        function readImg(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview-img').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);

                saveImg($('#img'));
            }
        }

        function update_data(value, column){
            var $_GET = <?php echo json_encode($_GET); ?>;
            
            $.ajax({
                url: "update-configuration.php",
                method: "POST",
                data: {valor: value, columna: column},
                success: function(results){
                    //alert(results);
                }
            });
        }

        $(document).on("blur", "#address", function(){
            var address = $(this).val();
            update_data(address, "address");
        })
        $(document).on("blur", "#phone", function(){
            var phone = $(this).val();
            update_data(phone, "phone");
        })
        $(document).on("blur", "#email", function(){
            var email = $(this).val();
            update_data(email, "email");
        })
        $(document).on("blur", "#facebook", function(){
            var facebook = $(this).val();
            update_data(facebook, "facebook");
        })
        $(document).on("blur", "#twitter", function(){
            var twitter = $(this).val();
            update_data(twitter, "twitter");
        })
        $(document).on("blur", "#linkedin", function(){
            var linkedin = $(this).val();
            update_data(linkedin, "linkedin");
        })
        

        $(document).ready(function() {
        
        });
    </script>

</body>

</html>