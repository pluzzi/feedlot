<?php include('islogin.php'); ?>

<?php
    $page_id = 0;
?>

<!DOCTYPE html>
<html>

<head>
    <?php include('headers.php'); ?>
</head>

<body>
    
    <!-- The Modal -->
    <div class="modal fade" id="modalInformacion">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h2 class="modal-title" id="modalInformacionTitle"></h2>
                    <h3 class="modal-title" id="modalInformacionFrom"></h3>
                    <h3 class="modal-title" id="modalInformacionName"></h3>
                </div>
                
                <!-- Modal body -->
                <div class="modal-body" id="modalInformacionBody" style="word-wrap: break-word;">
                
                </div>
                
                <!-- Modal footer -->
                <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
                </div>
                
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div id="wrapper">
                <?php include('nav-bar.php'); ?>

                <div id="page-wrapper" class="gray-bg dashbard-1">
                    <?php include('top-bar.php'); ?>

                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Mensajes</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>

                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dataTables-example" >
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Contacto</th>
                                            <th>E-Mail</th>
                                            <th>Título</th>
                                            <th>Fecha</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody id="results">
                                        
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                            
                </div>
            </div>

            <?php include('footer.php'); ?> 
        </div>
    </div>

    <?php include('scripts.php'); ?>

    <script>
        function getMessages(){
            $.ajax({
                url: "get-messages.php",
                method: "POST",
                success: function(results){
                    $("#results").html(results);
                }
            })
        }

        $(document).ready(function() {
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    
                ],
                searching: false,
                bInfo: false

            });
            
            // modal
            $(document).on("click", "#read", function(){
                var id = $(this).data('id');

                $.ajax({
                    url: "get-message.php",
                    method: "POST",
                    data: {id: id},
                    success: function(result){
                        //alert(result);
                        var message = JSON.parse(result);
                        $("#modalInformacionTitle").html(message['title']);
                        $("#modalInformacionFrom").html(message['email']);
                        $("#modalInformacionName").html(message['name']);
                        $("#modalInformacionBody").html(message['message']);
                        $("#modalInformacion").modal('show');
                    }
                })
            }) 
            
            getMessages();
        });
    </script>


</body>

</html>
