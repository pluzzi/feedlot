<?php include('islogin.php'); ?>

<?php
    $page_id = 6;
?>

<!DOCTYPE html>
<html>

<head>
    <?php include('headers.php'); ?>
</head>

<body>
    <div class="row">
        <div class="col-lg-12">
            <div id="wrapper">
                
                <?php include('nav-bar.php'); ?>

                <div id="page-wrapper" class="gray-bg dashbard-1">
                    <?php include('top-bar.php'); ?>
                    
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Nuevo Integrante</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>

                        <div class="ibox-content">
                            <div class="form-horizontal" >
                                <div class="form-group"><label class="col-sm-2 control-label">Nombre</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" id="name"></div>
                                </div>

                                <div class="hr-line-dashed"></div>

                                <div class="form-group"><label class="col-sm-2 control-label">Profesión</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" id="profession"></div>
                                </div>

                                <div class="hr-line-dashed"></div>

                                <div class="form-group"><label class="col-sm-2 control-label">Descripción</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" id="description"></div>
                                </div>

                                <div class="hr-line-dashed"></div>

                                <div class="form-group"><label class="col-sm-2 control-label">Facebook</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" id="facebook"></div>
                                </div>

                                <div class="hr-line-dashed"></div>

                                <div class="form-group"><label class="col-sm-2 control-label">Twitter</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" id="twitter"></div>
                                </div>

                                <div class="hr-line-dashed"></div>

                                <div class="form-group"><label class="col-sm-2 control-label">Linkedin</label>
                                    <div class="col-sm-10"><input type="text" class="form-control" id="linkedin"></div>
                                </div>

                                <!-- Cropper -->
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title back-change">
                                        <h5>Imagen</h5>
                                    </div>
                                    <div class="ibox-content">
                                        <p>
                                            Seleccione la imagen para el Integrante.
                                        </p>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h4>Preview</h4>
                                                <img class="img-preview img-preview-sm" id="preview"/>
                                                
                                                <div class="btn-group">
                                                    <label title="Upload image file" for="inputImage" class="btn btn-primary">
                                                        <input onchange="readURL(this);" accept="image/*" type="file"  id="team-img">
                                                    </label>
                                                    
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-4">
                                        <button class="btn btn-primary" id="save" >Guardar</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php include('footer.php'); ?>
        </div>
    </div>

    <?php include('scripts.php'); ?>

    <script src="js/plugins/summernote/summernote.min.js"></script>

    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $(document).ready(function() {
            $(document).on("click", "#save", function(){
                var name = $("#name").val();
                var profession = $("#profession").val();
                var description = $("#description").val();
                var facebook = $("#facebook").val();
                var twitter = $("#twitter").val();
                var linkedin = $("#linkedin").val();

                $.ajax({
                    url: "save-team.php",
                    method: "POST",
                    data: {name: name, profession: profession, description: description, facebook: facebook, twitter: twitter, linkedin: linkedin },
                    success: function(id){
                        var file_data = $("#team-img").prop('files')[0];   
                        var form_data = new FormData();                  
                        form_data.append('file', file_data);

                        $.ajax({
                            url: 'save-team-img.php?id='+id,
                            dataType: 'text',
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: form_data,                         
                            type: 'POST',
                            success: function(result){
                                window.location.href = "team.php";
                            }
                        });
                    }
                });
            })

        });
    </script>

</body>

</html>