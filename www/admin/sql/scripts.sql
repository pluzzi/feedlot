create table history(
	id int not null auto_increment,
    io_id int not null,
    value varchar(255) not null,
    create_time datetime not null,
    primary key(id)
);

create table io(
	id int not null auto_increment,
    sort_description varchar(255) not null,
    description varchar(1024) not null,
    primary key (id)
    
);

insert into io(sort_description, description)
values ('DIn1', 'Digital In 1'),
('DIn2', 'Digital In 2'),
('DIn3', 'Digital In 3'),
('DIn4', 'Digital In 4'),
('DIn5', 'Digital In 5'),
('DIn6', 'Digital In 6'),
('DOut1', 'Digital Out 1'),
('DOut2', 'Digital Out 2'),
('DOut3', 'Digital Out 3'),
('DOut4', 'Digital Out 4'),
('DOut5', 'Digital Out 5'),
('AIn', 'Analogic In'),
('ALR', 'Alarm');

use datalogger

select * from history

delete from history where id >= 1

select * from io

select NOW()


create table users(
	id int not null auto_increment,
    name varchar(255) not null,
    email varchar(1024) not null,
    password varchar(2048) not null,
    primary key (id)
    
)

insert into users(name, email, password)
values('Patricio', 'patricioluzzi2014@gmail.com', '123456789')



SELECT * FROM history h
LEFT JOIN io c on c.id = h.io_id
 ORDER BY h.create_time DESC LIMIT 8

drop table alarms

create table alarms(
	id int not null auto_increment,
    sort_description varchar(255) not null,
    description varchar(2048) not null,
    primary key(id)
)

insert into alarms(sort_description, description)
values('AI2','Descripcion de la alarma.')




