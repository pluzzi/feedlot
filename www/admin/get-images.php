<?php
    include('config/database-config.php');
        
    $sql = "select i.id, i.img, p.title from product_images i
            left join products p on p.id = i.productid
            where p.id=".$_GET['id'] ;

    $result = $conn->query($sql);

    while ($row = mysqli_fetch_assoc($result)) {
        echo '<tr>
                <td>'.$row['id'] .'</td>
                <td>'.$row['title'] .'</td>
                <td><img style="width: 100px; height: 70px !important;" class="img-preview img-preview-sm" src="data:image/jpeg;base64,'.base64_encode( $row['img'] ).'" /></td>
                <td>
                    <button id="delete" class="btn btn-primary btn-sm" data-id="'.$row['id'].'">
                        <i class="fa fa-minus-circle"></i>
                    </button>
                </td>
            </tr>';
    }

?>