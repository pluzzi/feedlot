<!DOCTYPE html>

<?php 
    $id = $_GET['id'];
    
    include('config/database-config.php');
    
    $sql = "select 
            id, title, description
            from products
            where id=".$id;

    $result = $conn->query($sql);
    $row = mysqli_fetch_assoc($result);
?>

<html lang="en">
    <head>
        <?php include('headers.php'); ?>
    </head>
    <body>
        <div id="preloader">
            <div id="preloader-inner"></div>
        </div><!--/preloader-->

        <?php include('top-bar.php'); ?>

        <div class="space-70"></div>

        <section id="content-region-3" class="padding-40 page-tree-bg">
            <div class="container">
                <h3 class="page-tree-text">
                    <?php echo $row['title']; ?>
                </h3>
            </div>
        </section><!--page-tree end here-->

        <div class="space-70"></div>

        <div class="container portfolio-details">
            <div class="row">
                <div class="col-md-8 portfolio-single-slide margin-btm-20">
                    <div id="js-grid-slider-thumbnail" class="cbp ">
                        <?php 
                            $sql = "select 
                                    id, img
                                    from product_images
                                    where productid=".$id;

                            $images = $conn->query($sql);

                            while($img = mysqli_fetch_assoc($images)){
                                echo '<div class="cbp-item">
                                        <div class="cbp-caption">
                                            <div class="cbp-caption-defaultWrap">
                                                <img style="width: 100%; height: 500px" alt="" src="data:image/jpeg;base64,'.base64_encode( $img['img'] ).'"/>
                                            </div>
                                        </div>
                                    </div>';
                            }
                        ?>
                    </div>
                    <div id="js-pagination-slider">
                        <?php 
                            $sql = "select 
                                    id, img
                                    from product_images
                                    where productid=".$id;

                            $images = $conn->query($sql);

                            while($img = mysqli_fetch_assoc($images)){
                                echo '<div class="cbp-pagination-item cbp-pagination-active">
                                        <img style="width: 100px; height: 70px" alt="" src="data:image/jpeg;base64,'.base64_encode( $img['img'] ).'"/>
                                    </div>';
                            }
                        ?>
                    </div>
                    <div class="space-20"></div>
                    <div class="portfolio-single-desc">
                        <h3>
                            <?php echo $row['title']; ?>
                        </h3>

                        <?php echo $row['description']; ?>
                       
                    </div><!--portfolio detail-->
                </div>
                <!--<div class="col-md-4">
                    <div class="portfolio-side-bar">
                        <p>
                            <strong>Client:</strong> Design_mylife
                        </p>
                        <p>
                            <strong>Skills:</strong> HTML5, CSS3, Bootstrap3, Jquery
                        </p>
                        <p>
                            <strong>Time:</strong> March 10 2015 - March 25 2015
                        </p>
                        <p>
                            <strong>Client Says:</strong> "Great services!, my project goes live and am very happy to pay design_mylife. sed do eiusmod tempor incididunt ut labore et dolore magna."
                        </p>
                        <br>
                        <p>
                            <a href="#" class="btn btn-radius theme-btn-color">Visit Project</a>
                        </p>
                    </div>
                </div>-->
            </div>
        </div><!--portfolio single container end-->

        <div class="space-70"></div>
        
        <?php include('footer.php'); ?>

        <?php include('scripts.php'); ?>

        <script src="assets/cubeportfolio/js/jquery.cubeportfolio.min.js"></script> 

        <script>
            (function ($, window, document, undefined) {
                'use strict';

                // init cubeportfolio
                $('#js-grid-slider-thumbnail').cubeportfolio({
                    layoutMode: 'slider',
                    drag: true,
                    auto: false,
                    autoTimeout: 5000,
                    autoPauseOnHover: true,
                    showNavigation: false,
                    showPagination: false,
                    rewindNav: true,
                    scrollByPage: true,
                    gridAdjustment: 'responsive',
                    mediaQueries: [{
                            width: 0,
                            cols: 1,
                        }],
                    gapHorizontal: 0,
                    gapVertical: 0,
                    caption: '',
                    displayType: 'fadeIn',
                    displayTypeSpeed: 400,
                    plugins: {
                        slider: {
                            pagination: '#js-pagination-slider',
                            paginationClass: 'cbp-pagination-active',
                        }
                    },
                });
            })(jQuery, window, document);
        </script>
    </body>
</html>