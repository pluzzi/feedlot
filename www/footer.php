<?php
    include('config/database-config.php');
                
    $sql = "select * from configuration limit 1";

    $result = $conn->query($sql);
    $row = mysqli_fetch_assoc($result);

?>

<div class="row">
    <div class="col-md-12 text-center footer-bottom">
        <a href="index.php">
            <?php
                echo '<img style="width: 200px;" alt="" src="data:image/jpeg;base64,'.base64_encode( $row['logo'] ).'"/>';
            ?>
        </a>
        <div class="space-20"></div>
        <span>&copy;2019. All right resrved. Design by El Ojo del Amo</span>
    </div>
</div><!--footer copyright row end-->