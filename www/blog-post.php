<!DOCTYPE html>

<?php 
    $id = $_GET['id'];

    include('config/database-config.php');
    
    $sql = "select 
            p.id, p.img, p.title, p.author, c.description, DATE_FORMAT(p.create_date, '%M %d %Y') as fecha, body, video
            from posts p left join post_category c on p.categoryid=c.id
            where p.id=".$id;

    $result = $conn->query($sql);
    $row = mysqli_fetch_assoc($result);

?>

<html lang="en">
    <head>
        <?php include('headers.php'); ?>
    </head>
    <body>
        <div id="preloader">
            <div id="preloader-inner"></div>
        </div><!--/preloader-->

        <?php include('top-bar.php'); ?>

        <div class="space-70"></div>

        <section id="content-region-3" class="padding-40 page-tree-bg">
            <div class="container">
                <h3 class="page-tree-text">
                    News Letter
                </h3>
            </div>
        </section><!--page-tree end here-->

        <div class="space-70"></div>

        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="blog-post-section">
                        <?php
                            if($row['video']!=""){
                                echo '<div class="blog-post-img">
                                        <iframe width="100%" height="500" alt="" src="'.$row['video'].'" ></iframe>
                                    </div>';
                            }else{
                                echo '<div class="blog-post-img">
                                    <img class="img-fluid" alt="" src="data:image/jpeg;base64,'.base64_encode( $row['img'] ).'" />
                                </div>';
                            }
                        ?>
                    
                        
                                                
                        <div class="blog-post-header">
                            <h3><a href="" class="hover-color"><?php echo $row['title']; ?></a></h3>
                        </div>
                        <div class="blog-post-info">
                            <span><a href="#" class="hover-color">by <?php echo $row['author']; ?></a> | on <?php echo $row['fecha']; ?> | <a href="#" class="hover-color"><?php echo $row['description']; ?></a> </span>
                        </div>
                        <div class="blog-post-detail" style="word-wrap: break-word;">
                            <?php echo $row['body']; ?>
                        </div>
                    </div><!--blog post section end-->

                    <div class="row">
                        <div class="col-md-12">
                            <div class="post-comment-wrapper clearfix">
                                <?php
                                    $sql = "select pc.id, pc.comment, c.name from post_comments pc
                                    left join contacts c on c.id=pc.contactid
                                    where pc.postid=".$id;
                        
                                    $result = $conn->query($sql);
                                ?>

                                <h3>Comentarios (<?php echo $result->num_rows; ?>)</h3>
                                <?php
                                    while ($row = mysqli_fetch_assoc($result)) {
                                        echo '<div class="comment-box clearfix" style="word-wrap: break-word;">
                                                <span>by '.$row['name'].'</span>
                                                <p>
                                                    '.$row['comment'].'
                                                </p>
                                            </div>';
                                    }
                                ?>
                                
                            </div><!--comment wrapper-->

                            <div class="space-40"></div>

                            <div class="comment-form-wrapper">
                                <h3>Deja un comentario</h3>
                                <div class="row">
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" placeholder="Name" id="name" >
                                    </div>
                                    <div class="col-md-6">
                                        <input type="email" class="form-control" placeholder="Email" id="email">
                                    </div>
                                    <div class="col-md-12">
                                        <textarea class="form-control" rows="7" placeholder="Comment" id="comment"></textarea>
                                    </div>
                                    <div class="col-md-12 text-right">
                                        <button class="btn btn-lg theme-btn-color" onclick="addComment()">Comentar</button>
                                    </div>
                                </div>
                            </div><!--comment form wrapper-->
                        </div>
                    </div><!--row-->

                    <div class="space-20"></div>

                    <!--<div class="clearfix">
                        <div class="float-right">
                            <nav aria-label="Page navigation example">
                                <ul class="pagination">
                                    <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                                    <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item"><a class="page-link" href="#">Next</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>-->

                </div><!--blog content-->
                <div class="col-md-4">
                    <!--<div class="sidebar-box">
                        <div class="widget-search">
                            <form class="search-form">
                                <input type="text" class="form-control" placeholder="search here...">
                                <i class="ion-search" data-toggle="tooltip" data-placement="top" title="" data-original-title="hit enter to search"></i>
                            </form>
                        </div>
                    </div>-->
                    <hr>
                    <div class="sidebar-box">
                        <h4>Categorias</h4>
                        <ul class="cat-list">
                            <?php
                                $sql = "select * from post_category";
                    
                                $result = $conn->query($sql);
                            
                                while ($row = mysqli_fetch_assoc($result)) {
                                    echo '<li><a href="" data-toggle="tooltip" data-placement="right" title="" data-original-title="10" class="hover-color" data-id="'.$row['id'].'">'.$row['description'].'</a></li>';
                                }
                            ?>
                        </ul>
                    </div>
                    <!--<div class="sidebar-box">
                        <h4>Text widget</h4>
                        <p>
                            Doloreiur quia commolu ptatemp dolupta oreprerum tibusam eumenis et consent accullignis dentibea autem inisita.
                        </p>
                    </div>-->
                    <hr>
                    <div class="sidebar-box">
                        <h4>posts recientes</h4>
                        <?php
                            $sql = "select img, DATE_FORMAT(create_date, '%M %d %Y') as fecha, id, title from posts
                                    where id != ".$id."
                                    order by create_date desc
                                    limit 3";
                        
                            $result = $conn->query($sql);
                        
                            while ($row = mysqli_fetch_assoc($result)) {
                                echo '<div class="recent">
                                        <span>
                                            <img class="img-fluid" alt="" src="data:image/jpeg;base64,'.base64_encode( $row['img'] ).'" />
                                        </span>
                                        <p><a href="blog-post.php?id='.$row['id'].'" class="hover-color">'.$row['title'].'</a></p>
                                        <span class="recent-date">On '.$row['fecha'].'</span>
                                    </div>';
                            }
                        ?>

                    </div>
                    <div class="clearfix"></div>
                    <hr>
                    <div class="sidebar-box">
                        <h4>siguenos</h4>
                        <ul class=" list-inline social-btn">
                            <li class="list-inline-item"><a href="#"><i class="ion-social-facebook" data-toggle="tooltip" data-placement="top" title="" data-original-title="Like On Facebook"></i></a></li>
                            <li class="list-inline-item"><a href="#"><i class="ion-social-twitter" data-toggle="tooltip" data-placement="top" title="" data-original-title="Follow On twitter"></i></a></li>
                            <li class="list-inline-item"><a href="#"><i class="ion-social-googleplus" data-toggle="tooltip" data-placement="top" title="" data-original-title="Follow On googleplus"></i></a></li>
                            <li class="list-inline-item"><a href="#"><i class="ion-social-pinterest" data-toggle="tooltip" data-placement="top" title="" data-original-title="pinterest"></i></a></li>
                            <li class="list-inline-item"><a href="#"><i class="ion-social-skype" data-toggle="tooltip" data-placement="top" title="" data-original-title="skype"></i></a></li>
                        </ul>
                    </div><hr>
                </div>
            </div>

        </div><!--blog full page content end here-->
        
        <div class="space-70"></div>
        
        <?php include('footer.php'); ?>

        <!--back to top-->
        <a href="#" class="scrollToTop"><i class="ion-android-arrow-dropup-circle"></i></a>
        
        <!-- jQuery plugins. -->
        <script src="assets/js/plugins/plugins.js"></script>
        <script src="assets/js/app.js"></script>
        <script src="assets/cubeportfolio/js/jquery.cubeportfolio.min.js"></script> 
        <script>
            (function ($, window, document, undefined) {
                'use strict';

                // init cubeportfolio
                $('#js-grid-slider-thumbnail').cubeportfolio({
                    layoutMode: 'slider',
                    drag: true,
                    auto: false,
                    autoTimeout: 5000,
                    autoPauseOnHover: true,
                    showNavigation: false,
                    showPagination: false,
                    rewindNav: true,
                    scrollByPage: true,
                    gridAdjustment: 'responsive',
                    mediaQueries: [{
                            width: 0,
                            cols: 1
                        }],
                    gapHorizontal: 0,
                    gapVertical: 0,
                    caption: '',
                    displayType: 'fadeIn',
                    displayTypeSpeed: 400,
                    plugins: {
                        slider: {
                            pagination: '#js-pagination-slider',
                            paginationClass: 'cbp-pagination-active'
                        }
                    }
                });
            })(jQuery, window, document);
        </script>

        <script>
            function addComment(){
                var name = $("#name").val();
                var email = $("#email").val();
                var comment = $("#comment").val();
                var id = '<?php echo $id; ?>';

                $.ajax({
                    url: "save-post-comment.php",
                    method: "POST",
                    data: {name: name, email: email, comment: comment, id: id},
                    success: function(result){
                        window.location.href = "blog-post.php?id="+id;
                    }
                });
            }
        
        </script>
    </body>
</html>