<section id="about" class="padd-section" >
    <div class="container">
        <div class="row intro-row">
            <div class="col-md-12 text-center wow animated fadeInUp">
                <h2>El estado de tu ganado, donde quieras, cuando quieras</h2>
                <h3 class="subtitle">Conocé datos importantes como el estrés calórico, estado sanitario, ubicación de los animales y visualizar la "hoja de vida" de cada animal de forma inviolable gracias a los datos respaldados en blockchain.</h3>
            </div>
            <div class="space-70"></div>
            <div class=" col-md-4">
                <div class="services-box">
                    <i class="ion-edit"></i>
                    <h1>Proyectos a medida</h1>
                    <p>
                        Cada producto se adapta a las necesidades de nuestros clientes.
                    </p>
                </div>
            </div><!--services box-->
            <div class=" col-md-4">
                <div class="services-box">
                    <i class="ion-laptop"></i>
                    <h1>Digitalización Ganadera</h1>
                    <p>
                        La información estará disponible para cualquier dispositivo y en todo momento.
                    </p>
                </div>
            </div><!--services box-->
            <div class="col-md-4">
                <div class="services-box">
                    <i class="ion-ios-locked"></i>
                    <h1>Trazabilidad y Seguridad</h1>
                    <p>
                        Los datos son respaldados en blockchain para asegurar que sean trazables e inviolables.
                    </p>
                </div>
            </div><!--services box-->
        </div><!--row end-->
    </div><!--intro with services end-->
</section><!--about section end-->