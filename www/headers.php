<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>El Ojo del Amo</title>
<!--plugins-->
<link href="assets/css/plugins/plugins.css" rel="stylesheet">
<!--master slider-->
<link rel="stylesheet" href="assets/masterslider/style/masterslider.css"/>
<link href="assets/masterslider/skins/default/style.css" rel='stylesheet'>
<link href="assets/css/master-slider-custom.css" rel="stylesheet" type="text/css">
<!--cube portfolio-->
<link href="assets/cubeportfolio/css/cubeportfolio.min.css" rel='stylesheet'>
<!--Custom css-->
<link href="assets/css/style.css" rel="stylesheet">