<section id="our-team" class="padding-40 page-tree-bg">
    <div class="container">
        <h3 class="page-tree-text">
            Conoce al equipo
        </h3>
    </div>
</section><!--page-tree end here-->

<div class="space-70"></div>
    <div class="container">
        <?php
            include('config/database-config.php');
                
            $sql = "select * from team";

            $result = $conn->query($sql);

            $i=1;
            $j=1;
            
            while ($row = mysqli_fetch_assoc($result)) {
                if($i==1){
                    echo '<div class="row">';
                }

                echo '<div class="col-lg-3 col-md-6  margin-btm-20">
                        <div class="person-section">
                            <img alt="" class="img-fluid" src="data:image/jpeg;base64,'.base64_encode( $row['img'] ).'"/>
                            <div class="person-desc">
                                <h3>'.$row['name'].'   <span><br>'.$row['profession'].'</span></h3>

                                <p>
                                    '.$row['description'].'
                                </p>
                                <ul class=" team list-inline social-btn">
                                    <li class="list-inline-item"><a href="'.$row['facebook'].'"><i class="ion-social-facebook" data-toggle="tooltip" data-placement="top" title="" data-original-title="Facebook"></i></a></li>
                                    <li class="list-inline-item"><a href="'.$row['twitter'].'"><i class="ion-social-twitter" data-toggle="tooltip" data-placement="top" title="" data-original-title="Twitter"></i></a></li>
                                    <li class="list-inline-item"><a href="'.$row['linkedin'].'"><i class="ion-social-linkedin" data-toggle="tooltip" data-placement="top" title="" data-original-title="Linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div><!--person section end-->
                    </div>';

                if($i==4 or $i % 4 == 0 or $j==$result->num_rows ){
                    echo '</div>';
                    $i = 0;
                }

                $i++;
                $j++;
            }
        ?>
    
    </div><!--team 3 col end-->
</div><!--Our team page end-->