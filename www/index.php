
<!DOCTYPE html>
<html>
    <head>
        <?php include('headers.php'); ?>
    </head>

    <body data-spy="scroll">
        <div id="preloader">
            <div id="preloader-inner"></div>
        </div>
        
        <?php include('top-bar-home.php'); ?>

        <?php include('home.php'); ?>

        <?php include('about.php'); ?>

        <?php //include('work.php'); ?>

        <?php include('services.php'); ?>

        <?php include('news.php'); ?>

        <?php include('our-team.php'); ?>

        <?php include('contact.php'); ?>

        <?php include('scripts.php'); ?>
        
    </body>
</html>
