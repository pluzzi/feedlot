#include <SoftwareSerial.h>

SoftwareSerial bt(11,12);

char NOMBRE[21] = "Lector";
char BPS = 4;


void setup(){
  bt.begin(9600);

  pinMode(13, OUTPUT);
  digitalWrite(13, HIGH);
  delay(4000);

  digitalWrite(13, LOW);

  bt.print("AT");
  delay(1000);

  bt.print("AT+NAME");
  bt.print(NOMBRE);
  delay(1000);
  
  bt.print("AT+BAUD");
  bt.print(BPS);
  delay(1000);

  bt.print("AT+PIN");
  bt.print(1234);
  delay(1000);
  
}

void loop(){
  digitalWrite(13, !digitalRead(13));
  delay(300);  
}
