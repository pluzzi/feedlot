/*
 * Initial Author: ryand1011 (https://github.com/ryand1011)
 *
 * Reads data written by a program such as "rfid_write_personal_data.ino"
 *
 * See: https://github.com/miguelbalboa/rfid/tree/master/examples/rfid_write_personal_data
 *
 * Uses MIFARE RFID card using RFID-RC522 reader
 * Uses MFRC522 - Library
 * -----------------------------------------------------------------------------------------
 *             MFRC522      Arduino       Arduino   Arduino    Arduino          Arduino
 *             Reader/PCD   Uno/101       Mega      Nano v3    Leonardo/Micro   Pro Micro
 * Signal      Pin          Pin           Pin       Pin        Pin              Pin
 * -----------------------------------------------------------------------------------------
 * RST/Reset   RST          9             5         D9         RESET/ICSP-5     RST
 * SPI SS      SDA(SS)      10            53        D10        10               10
 * SPI MOSI    MOSI         11 / ICSP-4   51        D11        ICSP-4           16
 * SPI MISO    MISO         12 / ICSP-1   50        D12        ICSP-1           14
 * SPI SCK     SCK          13 / ICSP-3   52        D13        ICSP-3           15
*/

#include <SPI.h>
#include <MFRC522.h>
#include <SoftwareSerial.h>   // Incluimos la librería  SoftwareSerial
#include <U8g2lib.h>  

#define RST_PIN         9           // Configurable, see typical pin layout above
#define SS_PIN          10          // Configurable, see typical pin layout above
#define TXD_PIN         11
#define RXT_PIN         12

MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance
SoftwareSerial BT(TXD_PIN,RXT_PIN);    // Definimos los pines RX y TX del Arduino conectados al Bluetooth

U8G2_ST7920_128X64_1_SW_SPI u8g2(U8G2_R0, /* clock=*/ 7, /* data=*/ 6, /* CS=*/ 5, /* reset=*/ 4);

String inStringHex = "";

//*****************************************************************************************//
void setup() {
  Serial.begin(9600);                                           // Initialize serial communications with the PC
  SPI.begin();                                                  // Init SPI bus
  mfrc522.PCD_Init();                                              // Init MFRC522 card
  BT.begin(9600);       // Inicializamos el puerto serie BT que hemos creado
  Serial.println(F("Read personal data on a MIFARE PICC:"));    //shows in serial that it is ready to read
  u8g2.begin();
  u8g2.enableUTF8Print();
}

//*****************************************************************************************//
void loop(){
  u8g2.firstPage();
  do {
    u8g2.setFont(u8g2_font_haxrcorp4089_tr);
    u8g2.setCursor(15, 10);
    u8g2.print("www.elojodelamo.com.ar");
    
    u8g2.setFont(u8g2_font_ncenB14_tr);
    u8g2.setCursor(15, 40);
    char copy[50];
    inStringHex.toCharArray(copy, 50);
    u8g2.print(copy);
  } while ( u8g2.nextPage() );

  
  // Revisamos si hay nuevas tarjetas  presentes
  if ( mfrc522.PICC_IsNewCardPresent()){
    //Seleccionamos una tarjeta
    if ( mfrc522.PICC_ReadCardSerial()){
      inStringHex = "";
      
      for (byte i = 0; i < mfrc522.uid.size; i++) {
        if(mfrc522.uid.uidByte[i] < 0x10){
          inStringHex += "0";
        }
        inStringHex += String(mfrc522.uid.uidByte[i], HEX);
      }
      
      inStringHex.toUpperCase();
            
      // Write Serial
      Serial.print(inStringHex);
      Serial.println();
      
      // Write BT
      BT.print(inStringHex);
      BT.println();
      
      // Terminamos la lectura de la tarjeta  actual
      mfrc522.PICC_HaltA();
    }
  }
}
