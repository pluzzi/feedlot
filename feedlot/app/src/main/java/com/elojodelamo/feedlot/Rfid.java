package com.elojodelamo.feedlot;

import com.google.gson.annotations.SerializedName;

public class Rfid {
    private int id;
    private String rfid;

    public int getId() {
        return id;
    }

    public String getRfid() {
        return rfid;
    }
}
