<?php

    $idEntrada = $_POST['idEntrada'];
    $desde = $_POST['desde'];
    $hasta = $_POST['hasta'];

    $curl = curl_init();
    
    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://sensify.com.ar/api/movimiento/GetHistorial?idSerie=64&idEntrada=".$idEntrada."&desde=".$desde."T00:00:00&hasta=".$hasta."T23:59:59",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => "",
      CURLOPT_HTTPHEADER => array(
        "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjU4ZTI4YmY1LWIzYTktNDY1My1iOTFlLTM3MTgxOGFjYmZmZCIsImlzcyI6ImZsYW1tYS5jb20uYXIiLCJhdWQiOiJmbGFtbWEuY29tLmFyIn0.JffFzuRsnPuYHr2PjVYpZs-dT97Nq3srO94EXt1jdVw",
        "Content-Type: application/json"
      )
    ));
    
    $response = curl_exec($curl);

    $err = curl_error($curl);
    
    curl_close($curl);
    
    if ($err) {
        echo "cURL Error #:" . $err;
    } else {
        $result = json_decode($response);
     
        $arr = [];

        foreach ($result as $row){
            array_push($arr, $row->{'valor'});
        }

        $json['history'] = $arr;

        $json['value'] = end($arr);

        echo json_encode($json);

    }
    

?>