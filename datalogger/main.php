<?php include('islogin.php'); ?>

<?php
    $page_id = 0;
?>

<!DOCTYPE html>
<html>

<head>
    <?php include('headers.php'); ?>
</head>

<body>
    <div class="row">
        <div class="col-lg-12">
            <div id="wrapper">
                <?php include('nav-bar.php'); ?>

                <div id="page-wrapper" class="gray-bg dashbard-1">
                    <?php include('top-bar.php'); ?>

                    <div class="row">
                        <div class="col-lg-6">
                            <h2>Establecimiento La Filomena</h2>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="ibox">
                                        <div class="ibox-content">
                                            <h4>Filtros de fechas</h4>
                                            <div class="row form-group" id="dp-desde">
                                                <label class="col-sm-2 control-label">Desde</label>
                                                <div class="col-sm-9">
                                                    <div class="input-group date">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                        <input type="text" class="form-control" id="desde">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row form-group" id="dp-hasta">
                                                <label class="col-sm-2 control-label">Hasta</label>
                                                <div class="col-sm-9">
                                                    <div class="input-group date">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                        <input type="text" class="form-control" id="hasta">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="ibox">
                                        <div class="ibox-content">
                                            <h4>Historial de ITH</h4>
                                            <div class="widget style1 blue-bg">
                                                <div class="row vertical-align">
                                                    <div class="col-xs-3">
                                                        <i class="fa fa-calculator fa-3x"></i>
                                                    </div>
                                                    <div class="col-xs-9 text-right">
                                                        <h2 class="font-bold" id="ith"></h2>
                                                    </div>
                                                </div>
                                            </div>                            
                                            <div id="sparklineIth"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="ibox">
                                        <div class="ibox-content">
                                            <h4>Historial de Temperatura</h4>
                                            <div class="widget style1 navy-bg">
                                                <div class="row vertical-align">
                                                    <div class="col-xs-3">
                                                        <i class="fa fa-thermometer-half fa-3x"></i>
                                                    </div>
                                                    <div class="col-xs-9 text-right">
                                                        <h2 class="font-bold" id="temperature"></h2>
                                                    </div>
                                                </div>
                                            </div>                            
                                            <div id="sparklineTemperature"></div>


                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="ibox">
                                        <div class="ibox-content">
                                            <h4>Historial de Humedad</h4>
                                            <div class="widget style1 lazur-bg">
                                                <div class="row vertical-align">
                                                    <div class="col-xs-3">
                                                        <i class="fa fa-cloud fa-3x"></i>
                                                    </div>
                                                    <div class="col-xs-9 text-right">
                                                        <h2 class="font-bold" id="humidity"></h2>
                                                    </div>
                                                </div>
                                            </div>                            
                                            <div id="sparklineHumidity"></div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Estado de la Bomba
                                        </div>
                                        <div class="panel-body">
                                            <p>
                                                <h3>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <input type="checkbox" class="js-switch-rociador" id="rociadores" />
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div id="label-rociadores"></div>
                                                        </div>
                                                    </div>
                                                </h3>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Estado de la Válvula 1
                                        </div>
                                        <div class="panel-body">
                                            <p>
                                                <h3>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <input type="checkbox" class="js-switch-valvula-1" id="valvula1" />
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div id="label-valvula1"></div>
                                                        </div>
                                                    </div>
                                                </h3>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                        Estado de la Válvula 2
                                        </div>
                                        <div class="panel-body">
                                            <p>
                                                <h3>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <input type="checkbox" class="js-switch-valvula-2" id="valvula2" />
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div id="label-valvula2"></div>
                                                        </div>
                                                    </div>
                                                </h3>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Alertas
                                        </div>
                                        <div class="panel-body">
                                        <table class="table table-stripped medium">
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-circle fa-2x" id="status-icon1"></i>
                                                    </td>
                                                    <td id="status1">
                                                        No estres calórico
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-circle fa-2x" id="status-icon2"></i>
                                                    </td>
                                                    <td id="status2">
                                                        Leve estres calórico
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-circle fa-2x" id="status-icon3"></i>
                                                    </td>
                                                    <td id="status3">
                                                        Estres calórico medio
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <i class="fa fa-circle fa-2x" id="status-icon4"></i>
                                                    </td>
                                                    <td id="status4">
                                                        Estres calórico grave
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <h3 class="font-bold no-margins" id="sugerencia"></h3>
                                        </div>
                                    </div>
                                

                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <?php include('footer.php'); ?>    
                            
                </div>
            </div>
        </div>
    </div>

    <?php include('scripts.php'); ?>

    <script>
        function update(){
            var desde = $('#desde').val();
            var hasta = $('#hasta').val();
            
            //Temperatura: idEntrada 1
            //Humedad: idEntrada 2
            //Bomba: idEntrada 10
            //Valvula 1: idEntrada 68
            //Valvula 2: idEntrada 69
            //ITH: idEntrada 70
            var idEntrada = 1

            $.ajax({
                data: { desde: desde, hasta: hasta, idEntrada: idEntrada },
                type: "POST",
                url: "get-data.php",
            }).done(function( data) {
                console.log(data);

                var arr = JSON.parse(data)['history'] ;

                $("#sparklineTemperature").sparkline(arr, {
                    type: 'line',
                    width: '100%',
                    height: '60',
                    lineColor: '#1ab394',
                    fillColor: "#ffffff"
                });

                var value =  JSON.parse(data)['value'];
                $("#temperature").text(value + ' °C');

            });

            idEntrada = 2

            $.ajax({
                data: { desde: desde, hasta: hasta, idEntrada: idEntrada },
                type: "POST",
                url: "get-data.php",
            }).done(function( data) {
                console.log(data);

                var arr = JSON.parse(data)['history'] ;

                $("#sparklineHumidity").sparkline(arr, {
                    type: 'line',
                    width: '100%',
                    height: '60',
                    lineColor: '#1ab394',
                    fillColor: "#ffffff"
                });

                var value =  JSON.parse(data)['value'];
                $("#humidity").text(value + ' %');

            });

            idEntrada = 70

            $.ajax({
                data: { desde: desde, hasta: hasta, idEntrada: idEntrada },
                type: "POST",
                url: "get-data.php",
            }).done(function( data) {
                console.log(data);

                var arr = JSON.parse(data)['history'] ;

                $("#sparklineIth").sparkline(arr, {
                    type: 'line',
                    width: '100%',
                    height: '60',
                    lineColor: '#1ab394',
                    fillColor: "#ffffff"
                });

                var value =  JSON.parse(data)['value'];
                $("#ith").text(value);

                $("#status1").removeClass("navy-bg");
                $("#status2").removeClass("blue-bg");
                $("#status3").removeClass("yellow-bg");
                $("#status4").removeClass("red-bg");
                $("#status-icon1").removeClass("csm-color-success");
                $("#status-icon2").removeClass("csm-color-info");
                $("#status-icon3").removeClass("csm-color-warning");
                $("#status-icon4").removeClass("csm-color-danger");
                
                if(value <= 74){
                    $("#status1").addClass("navy-bg");
                    $("#status-icon1").addClass("csm-color-success");
                }
                if(value > 74 && value < 80 ){
                    $("#status2").addClass("blue-bg");
                    $("#status-icon2").addClass("csm-color-info");
                }
                if(value >= 80 && value < 84 ){
                    $("#status3").addClass("yellow-bg");
                    $("#status-icon3").addClass("csm-color-warning");
                }
                if(value >= 84 ){
                    $("#status4").addClass("red-bg");
                    $("#status-icon4").addClass("csm-color-danger");
                }

            });

            idEntrada = 10

            $.ajax({
                data: { idEntrada: idEntrada },
                type: "POST",
                url: "get-last-value.php",
            }).done(function( data) {
                console.log(data);
                var valor = JSON.parse(data)['valor'];
                toggleSwitch($('.js-switch-rociador'), $('#label-rociadores'), valor);
            });

            idEntrada = 68

            $.ajax({
                data: { idEntrada: idEntrada },
                type: "POST",
                url: "get-last-value.php",
            }).done(function( data) {
                console.log(data);
                var valor = JSON.parse(data)['valor'];
                toggleSwitch($('.js-switch-valvula-1'), $('#label-valvula1'), valor);
            });

            idEntrada = 69

            $.ajax({
                data: { idEntrada: idEntrada },
                type: "POST",
                url: "get-last-value.php",
            }).done(function( data) {
                console.log(data);
                var valor = JSON.parse(data)['valor'];
                toggleSwitch($('.js-switch-valvula-2'), $('#label-valvula2'), valor);
            });

        };

        $(document).ready(function() {
            var elem = document.querySelector('.js-switch-rociador');
            var switchery = new Switchery(elem, { color: '#1AB394' });
            $('.js-switch-rociador').trigger('click')

            var elem = document.querySelector('.js-switch-valvula-1');
            var switchery = new Switchery(elem, { color: '#1AB394' });
            $('.js-switch-valvula-1').trigger('click')

            var elem = document.querySelector('.js-switch-valvula-2');
            var switchery = new Switchery(elem, { color: '#1AB394' });
            $('.js-switch-valvula-2').trigger('click')

            var date = (new Date()).toISOString().split('T')[0];

            $('#desde').val(date);
            $('#hasta').val(date);

            update();

            setInterval(update, 15000);

        });

        function toggleSwitch(switch_elem, lbl_elem, on) {
            if (on==1){ // turn it on
                if ($(switch_elem)[0].checked){ // it already is so do 
                    // nothing
                }else{
                    $(switch_elem).trigger('click').attr("checked", "checked"); // it was off, turn it on
                    $(lbl_elem).text('Activado');
                }
            }else{ // turn it off
                if ($(switch_elem)[0].checked){ // it's already on so 
                    $(switch_elem).trigger('click').removeAttr("checked"); // turn it off
                    $(lbl_elem).text('Desactivado');
                }else{ // otherwise 
                    // nothing, already off
                }
            }
        }

        $('#dp-desde .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: "yyyy-mm-dd"
        });

        $('#dp-hasta .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: "yyyy-mm-dd"
        });

        $("#desde").change(function(){
            update();
        });
        $("#hasta").change(function(){
            update();
        });

    </script>

</body>

</html>
