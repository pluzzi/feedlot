<?php include('islogin.php'); ?>

<?php
    $page_id = 1;
?>

<!DOCTYPE html>
<html>

<head>
    <?php include('headers.php'); ?>
</head>

<body>
    <div class="row">
        <div class="col-lg-12">
            <div id="wrapper">
                
                <?php include('nav-bar.php'); ?>

                <div id="page-wrapper" class="gray-bg dashbard-1">
                    <?php include('top-bar.php'); ?>
                    
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Usuarios</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>

                        <div class="ibox-content">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dataTables-example" >
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Nombre</th>
                                            <th>E-Mail</th>
                                            <th>Password</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody id="results">
                                        
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <?php include('footer.php'); ?>
        </div>
    </div>

    <?php include('scripts.php'); ?>

    <script>
        function getUsers(){
            $.ajax({
                url: "get-users.php",
                method: "POST",
                success: function(results){
                    $("#results").html(results);
                }
            })
        }

        function update_data(id, dato, columna){
            $.ajax({
                url: "update-user.php",
                method: "POST",
                data: {user: id, valor: dato, col: columna},
                success: function(results){
                    getDatos();
                }
            })
        }

        $(document).ready(function() {
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    
                ]

            });

            $(document).on("blur", "#nameUpdate", function(){
                update_data($(this).data("id"), $(this).text(), "name");
            });
            $(document).on("blur", "#emailUpdate", function(){
                update_data($(this).data("id"), $(this).text(), "email");
            });
            $(document).on("blur", "#passwordUpdate", function(){
                update_data($(this).data("id"), $(this).text(), "password");
            });

            $(document).on("click", "#agregar", function(){
                var nameAdd = $("#nameAdd").text();
                var emailAdd = $("#emailAdd").text();
                var passwordAdd = $("#passwordAdd").text();
                
                if(nameAdd!="" && emailAdd!="" && passwordAdd!=""){
                    $.ajax({
                        url: "insert-user.php",
                        async: false,
                        method: "POST",
                        data: {
                            name: nameAdd, 
                            email: emailAdd,
                            password: passwordAdd
                        },
                        success: function(results){
                            getUsers();
                        }
                    })
                }
                
                
            });

            $(document).on("click", "#delete", function(){
                var id = $(this).data("id");

                $.ajax({
                    url: "delete-user.php",
                    method: "POST",
                    data: {user: id},
                    success: function(results){
                        getUsers();
                    }
                })
            })

            getUsers();

        });
    </script>

</body>

</html>