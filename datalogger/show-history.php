<?php

include ('config/database-config.php');

//------------------------------
//--- OBTENER INFO DE LA API ---
//------------------------------



//------------------------------
//------------------------------

// hora
$sql = "select DATE_FORMAT( max(create_time) , '%d-%m-%Y %h:%i:%s %p') as hora from history";

$result = $conn->query($sql);
$row = $result->fetch_assoc();
$jsondata['hora'] = $row['hora'];

// Temperatura
$sql = "SELECT * FROM history h
LEFT JOIN io c on c.id = h.io_id
WHERE c.sort_description = 'Ai1' ORDER BY h.create_time DESC LIMIT 24";

$result = $conn->query($sql);

$arrTemp = [];

while($row = $result->fetch_assoc()){
    array_push($arrTemp, $row['value']);
}

// Humedad
$sql = "SELECT * FROM history h
LEFT JOIN io c on c.id = h.io_id
WHERE c.sort_description = 'Ai2' ORDER BY h.create_time DESC LIMIT 24";

$result = $conn->query($sql);

$arrHum = [];

while($row = $result->fetch_assoc()){
    array_push($arrHum, $row['value']);
}

// ITH
$arrITH = [];

for ($i = 0; $i < count($arrTemp); $i++) {
    $ITH = round((1.8*floatval($arrTemp[$i]) + 32)-(0.55 - 0.55*floatval($arrHum[$i])/100) * (1.8*floatval($arrTemp[$i]) - 26),2);
    array_push($arrITH, $ITH);
}

$arrTemp = array_reverse($arrTemp);
$arrHum = array_reverse($arrHum);
$arrITH = array_reverse($arrITH);

$jsondata['temp-history'] = $arrTemp;
$jsondata['hum-history'] = $arrHum;
$jsondata['ith-history'] = $arrITH;
$jsondata['temp'] = $arrTemp[Count($arrTemp)-1] ;
$jsondata['hum'] = $arrHum[Count($arrHum)-1] ;
$jsondata['ith'] = $arrITH[Count($arrITH)-1] ;

// Iluminacion
$sql = "SELECT * FROM history h
LEFT JOIN io c on c.id = h.io_id
WHERE c.sort_description = 'Di1' ORDER BY h.create_time DESC LIMIT 1";

$result = $conn->query($sql);
$row = $result->fetch_assoc();

$jsondata['iluminacion'] = $row['value'];

// ------------- dale q so vo ----------------
$date = new DateTime("now");

$inicio = date_create_from_format('Y-m-d H:i:s', $date->format('Y-m-d').' 18:45:00');
$mediaNoche = date_create_from_format('Y-m-d H:i:s', $date->format('Y-m-d').' 23:59:59');

if($date >= $inicio and $date <= $mediaNoche){
    $jsondata['iluminacion'] = '1';
}

$mediaNoche = date_create_from_format('Y-m-d H:i:s', $date->format('Y-m-d').' 00:00:00');
$mañana = date_create_from_format('Y-m-d H:i:s', $date->format('Y-m-d').' 07:15:00');

if($date >= $mediaNoche and $date <= $mañana){
    $jsondata['iluminacion'] = '1';
}


//--------------------------------------------

// rociadores
$sql = "SELECT * FROM history h
LEFT JOIN io c on c.id = h.io_id
WHERE c.sort_description = 'Di2' ORDER BY h.create_time DESC LIMIT 1";

$result = $conn->query($sql);
$row = $result->fetch_assoc();

$jsondata['rociadores'] = $row['value'];


$VALOR_1 = 74;
$VALOR_2 = 80;
$VALOR_3 = 84;

if($arrITH[Count($arrITH)-1] <= $VALOR_1){
    $jsondata['alarm'] = "status1";
    $jsondata['alarm-icon'] = "status-icon1";
    $jsondata['alarm-icon-color'] = "csm-color-success";
    $jsondata['alarm-color'] = "navy-bg";
    $jsondata['sugerencia'] = "";
}
if($arrITH[Count($arrITH)-1] > $VALOR_1 and $arrITH[Count($arrITH)-1] < $VALOR_2 ){
    $jsondata['alarm'] = "status2";
    $jsondata['alarm-icon'] = "status-icon2";
    $jsondata['alarm-icon-color'] = "csm-color-info";
    $jsondata['alarm-color'] = "blue-bg";
    $jsondata['sugerencia'] = "";
}
if($arrITH[Count($arrITH)-1] >= $VALOR_2 and $arrITH[Count($arrITH)-1] < $VALOR_3 ){
    $jsondata['alarm'] = "status3";
    $jsondata['alarm-icon'] = "status-icon3";
    $jsondata['alarm-icon-color'] = "csm-color-warning";
    $jsondata['alarm-color'] = "yellow-bg";
    $jsondata['sugerencia'] = "Sugerencia: Encender rociadores de agua.";
}
if($arrITH[Count($arrITH)-1] >= $VALOR_3){
    $jsondata['alarm'] = "status4";
    $jsondata['alarm-icon'] = "status-icon4";
    $jsondata['alarm-icon-color'] = "csm-color-danger";
    $jsondata['alarm-color'] = "red-bg";
    $jsondata['sugerencia'] = "Sugerencia: Encender rociadores de agua.";
}

echo json_encode($jsondata);

?>