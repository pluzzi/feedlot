import logging
import time
from daemon import runner
import socket
import mysql.connector
import random

class App():
   def __init__(self):
      self.stdin_path      = '/dev/null'
      self.stdout_path     = '/dev/ttyS0'
      self.stderr_path     = '/dev/ttyS0'
      self.pidfile_path    =  '/var/www/tcp/tcpdaemon.pid'
      self.pidfile_timeout = 5

   def run(self):
        try:
            mydb = mysql.connector.connect(
                host="localhost",
                user="root",
                passwd="Fi11235813**",
                database="datalogger"
            )

            TCP_IP = '0.0.0.0'
            TCP_PORT = 1025
            BUFFER_SIZE = 1024  # Normally 1024, but we want fast response

            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            s.bind((TCP_IP, TCP_PORT))
            s.listen(1)
            s.settimeout(120)

            logger.info('Tcp Server Start')
        except Exception, e:
            logger.error(str(e))

        while 1:
            try:
                logger.info('Waiting connection...')
                conn, addr = s.accept()
                #print 'Connection address:', addr
                logger.info('Connection address: ' + str(addr))
                
                data = conn.recv(BUFFER_SIZE)
                #print 'received data:', data
                logger.info('received data:' + str(data))

                #GET Ai1:24.40;Ai2:70.40;Ai3:Spare;Ai4:Spare;Di1:1;Di2:0;Di3:0;Di4:0;Di5:0;Di6:0;Di7:0;Di8:0

                dataSplited = data.replace('GET ', '').replace('Spare', '0').split(';')

                for data in dataSplited:
                    param = data.split(':')[0]
                    value = data.split(':')[1]

                    logger.info(param + ': ' + value)

                    mycursor = mydb.cursor()

                    sql = "select id from io where sort_description = '"+param+"'"
                    mycursor.execute(sql)
                    
                    myresult = mycursor.fetchall()
                    
                    if len(myresult)!=0:
                        id = myresult[0][0]

                        sql = "INSERT INTO history (io_id, value, create_time) VALUES ('"+str(id)+"', '" + value + "', NOW())"
                        mycursor.execute(sql)
                        mydb.commit()
                        #print(mycursor.rowcount, "record inserted.")
                        logger.info(str(mycursor.rowcount) + " record inserted.")
                
                conn.close()
            except Exception, e:
                logger.error(str(e))
                #conn.close()
                


if __name__ == '__main__':
   app = App()
   logger = logging.getLogger("tcpdaemonlog")
   logger.setLevel(logging.INFO)
   formatter = logging.Formatter("%(asctime)s - %(message)s")
   handler = logging.FileHandler("/var/www/tcp/tcpdaemon.log")
   handler.setFormatter(formatter)
   logger.addHandler(handler)

   serv = runner.DaemonRunner(app)
   serv.daemon_context.files_preserve=[handler.stream]
   serv.do_action()