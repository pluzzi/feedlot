import datetime
import mysql.connector
import requests
import json
import time

print str(datetime.datetime.now()) + ' - HISTORY SCRIPT Started'

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  passwd="Fi11235813*",
  database="datalogger"
)

while(1):
    url = 'https://sensify.com.ar/api/account/login'
    data = {'Email': 'elojodelamo@sensify.com.ar', 'Password': 'kj2n4k24n@rt!#fw#gd43&f3e'}
    headers = {'Content-type': 'application/json'}
    response = requests.post(url=url, json=data, headers=headers)
    data = response.json()
    token = data['token']

    url = 'https://sensify.com.ar/api/movimiento/GetUltimosMovimientosByIDSerie/63'
    data = {'token': token}
    headers = {'Content-type': 'application/json', 'Authorization': 'Bearer {}'.format(token)}
    response = requests.post(url=url, json=data, headers=headers)
    data = response.json()

    response_dict = json.loads(response.text)

    humedad = [x for x in response_dict if x['idEntrada'] == 34][0]
    temperatura = [x for x in response_dict if x['idEntrada'] == 40][0]

    mycursor = mydb.cursor()

    sql = "select id from io where sort_description = 'Ai1'"
    mycursor.execute(sql)
    myresult = mycursor.fetchall()
    id = myresult[0][0]

    sql = "INSERT INTO history (io_id, value, create_time) VALUES ('"+str(id)+"', '" + str(humedad['valor']) + "', NOW())"
    mycursor.execute(sql)
    mydb.commit()

    sql = "select id from io where sort_description = 'Ai2'"
    mycursor.execute(sql)
    myresult = mycursor.fetchall()
    id = myresult[0][0]

    sql = "INSERT INTO history (io_id, value, create_time) VALUES ('"+str(id)+"', '" + str(temperatura['valor']) + "', NOW())"
    mycursor.execute(sql)
    mydb.commit()

    print("Se guardaron los datos correctamente.")

    time.sleep(60)