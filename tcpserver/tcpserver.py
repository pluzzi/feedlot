import socket
import datetime
import mysql.connector
import random

print str(datetime.datetime.now()) + ' - TCPSERVER Start'

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  passwd="Fi11235813**",
  database="datalogger"
)

TCP_IP = '0.0.0.0'
TCP_PORT = 1025
BUFFER_SIZE = 1024  # Normally 1024, but we want fast response

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((TCP_IP, TCP_PORT))
s.listen(1)



while 1:
    conn, addr = s.accept()
    print str(datetime.datetime.now()) + ' - ' + 'Connection address:' + str(addr)
        
    data = conn.recv(BUFFER_SIZE)
    print str(datetime.datetime.now()) + ' - ' + data
    
    ranTemp = random.randint(-100, 100)
    ranHum = random.randint(0, 100)

    dataSplited = data.replace('"', ' ').replace('000000000000001', '').replace('-HHHH', str(ranTemp) +','+ str(ranHum) ).split(' ')

    arr = []
    bandera = False

    for var in dataSplited:
        var = var.replace(':', '').strip()
        
        if(var != ''):
            if(bandera):
                arr.append(var)
                bandera = False
            else:
                arr.append(var.replace(',',''))

            if(var.find('AIn')!=-1):
                bandera = True

    for i in range(0, len(arr)-1, 2):
        mycursor = mydb.cursor()

        sql = "select id from io where sort_description = '"+arr[i]+"'"
        mycursor.execute(sql)
        myresult = mycursor.fetchall()
        id = myresult[0][0]

        sql = "INSERT INTO history (io_id, value, create_time) VALUES ('"+str(id)+"', '" + arr[i+1] + "', NOW())"
        mycursor.execute(sql)
        mydb.commit()
    
    conn.close()